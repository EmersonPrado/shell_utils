#!/bin/bash

# fullest_dirs_1_fs.sh - Finds directories with more content in one filesystem
# More content - Number of files or total storage space per directory
# Ignores subdirectories inside other filesystems with intelligence beyond
# `find -xdev` and `du -s`
# Outputs directories in ascending size

error () {
  local status=$1
  shift
  printf '%s\n' "$@" >&2
  # shellcheck disable=SC2086
  exit $status
}

help () {
  echo 'Usage:'
  echo "$(basename "$0") [ -i ] <Directory>"
  printf '\tDirectory - Anyone inside the filesystem to measure\n'
  printf '\t-i - Measure files (inodes) count instead of disk occupation\n'
}

case "$1" in
  -h) help ;;
  -i) du_flag='--inodes' ; shift ;;
  *)  du_flag='-h' ;;
esac
(($# == 1)) || error 1 "$(help)"
[[ -d "$1" ]] || error 2 "Invalid directory: $1"

mount_point="$(df --output=target "$1" | tail -1)"
case "$mount_point" in
  /)  ignore_fs_filter='^/.'              ;;
  /*) ignore_fs_filter="^${mount_point}/" ;;
  *)  error 3 'Mount point not found'     ;;
esac

# shellcheck disable=SC2046
find "$mount_point" -xdev -type d $(
  findmnt -lno target |
  sed -n "\\#$ignore_fs_filter#s/^/! -path /p"
) -print0 |
xargs -0I% du -xsS $du_flag "%" |
sort -h
